import { Http, RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class MovieService {

  url = 'http://localhost:3000/movie';

  constructor(private http: Http) {

  }

  getMovies() {
    return this.http.get(this.url);
  }

  getMovieDetails(id: number) {
    return this.http.get(this.url + '/' + id);
  }

  addMovie(title: string, year: string, directors: string,
      writers: string, stars: string, genre: string,
      shortDescription: string, storyline: string,
      length: string, rating: number, selectedFile: any) {

      const body = new FormData();
      body.append('title', title);
      body.append('year', year);
      body.append('directors', directors);
      body.append('writers', writers);
      body.append('stars', stars);
      body.append('genre', genre);
      body.append('shortDescription', shortDescription);
      body.append('storyline', storyline);
      body.append('length', length);
      body.append('rating', '' + rating);
      body.append('thumbnail', selectedFile);

      return this.http.post(this.url, body);

      /*
      const body = {
        title: title,
        year: year,
        directors: directors,
        writers: writers,
        stars: stars,
        genre: genre,
        shortDescription: shortDescription,
        storyline: storyline
      };

      const header = new Headers({ 'Content-Type': 'application/json' });
      const requestOption = new RequestOptions({ headers: header });

      return this.http.post(this.url, body, requestOption);
      */
  }

  updateMovie() {

  }

  deleteMovie(id: number) {
    return this.http.delete(this.url + '/' + id);
  }

}

import { Component, OnInit } from '@angular/core';
import { MovieService } from '../movie.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-movie',
  templateUrl: './add-movie.component.html',
  styleUrls: ['./add-movie.component.css']
})
export class AddMovieComponent implements OnInit {
  title = '';
  year = '';
  directors = '';
  writers = '';
  stars = '';
  genre = '';
  shortDescription = '';
  storyline = '';
  length = '';
  rating = 0;
  imageUrl: any;
  selectedFile: any;

  constructor(
    private router: Router,
    private movieService: MovieService) { }

  ngOnInit() {
  }

  onAdd() {
    // console.log(`title: ${this.title}`);
    // console.log(`Year: ${this.year}`);
    // console.log(`directors: ${this.directors}`);
    // console.log(`writers: ${this.writers}`);
    // console.log(`stars: ${this.stars}`);
    // console.log(`genre: ${this.genre}`);
    // console.log(`shortDescription: ${this.shortDescription}`);
    // console.log(`storyline: ${this.storyline}`);

    this.movieService
      .addMovie(this.title, this.year, this.directors, this.writers, this.stars,
          this.genre, this.shortDescription, this.storyline, this.length,
          this.rating, this.selectedFile)
      .subscribe(response => {
        console.log(response);
        const body = response.json();
        if (body['status'] == 'success') {
          // go to the list of movies
          this.router.navigate(['/movie-list']);
        } else {
          alert('error while adding movie');
        }
      });
  }

  onCancel() {
    this.router.navigate(['/movie-list']);
  }

  onChange($event) {
    this.selectedFile = $event.target.files[0];
    const fileReader = new FileReader();
    fileReader.onload = () => {
      this.imageUrl = fileReader.result;
    };
    fileReader.readAsDataURL(this.selectedFile);
  }

}

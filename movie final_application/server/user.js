const express = require('express');
const db = require('./db');
const utils = require('./utils');
const multer = require('multer');
const upload = multer({dest: 'images/'});

const router = express.Router();

router.post('/user/profileImage/:id', upload.single('photo'), (request, response) => {
    console.log('file uploaded at: ' + request.file.filename);
    
    const id = request.params.id;
    const statement = `update User set profileImage = '${request.file.filename}' where id = ${id}`;
    const connection = db.connect();
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    });
});

router.post('/user/signup', (request, response) => {
    const { name, email, password, phone } = request.body;

    const statement = `
        insert into User 
            (name, email, password, phone) 
            values 
            ('${name}', '${email}', '${password}', '${phone}');
    `;
    const connection = db.connect();
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    });
});

router.get('/user/profile/:id', (request, response) => {
    const id = request.params.id;
    const statement = `select * from User where id = ${id}`;
    const connection = db.connect();
    connection.query(statement, (error, result) => {
        connection.end();
        if (result.length == 0) {
            response.send(utils.createResponse('user not found', 'user not found'));
        } else {
            const user = result[0];
            response.send(utils.createResponse(error, {
                id: user.id,
                name: user.name,
                email: user.email,
                phone: user.phone,
                profileImage: user.profileImage
            }));
        }
    });
});




router.post('/user/signin', (request, response) => {
    const { email, password } = request.body;

    const statement = `select * from User where email = '${email}' and password = '${password}'`;
    const connection = db.connect();
    connection.query(statement, (error, result) => {
        connection.end();

        let status = '';
        let data = null;
        if (error == null) {
            // query got executed successfully
            if (result.length == 0) {
                // error
                status = 'error';
                data = 'Invalid user email or password';
            } else {
                // success
                status = 'success';
                data = result[0]; // send the user details
            }
        } else {
            // error in the statement
            status = 'error';
            data = error;
        }
        response.send({
            status: status,
            data: data
        });
    });
});


module.exports = router;
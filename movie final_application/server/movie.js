const express = require('express');
const db = require('./db');
const utils = require('./utils');
const multer = require('multer');
const upload = multer({dest: 'images/'});

const router = express.Router();

router.get('/movie', (request, response) => {
    const statement = `select id, title, year, rating, thumbnail, length, directors, genre, writers from Movie`;
    const connection = db.connect();
    connection.query(statement, (error, movies) => {
        connection.end();
        response.send(utils.createResponse(error, movies));
    });
});

router.get('/movie/search/:text', (request, response) => {
    const text = request.params.text;
    const statement = `select id, title, year, rating, thumbnail, length, directors, genre, writers from Movie where LOWER(title) like '%${text}%'`;
    const connection = db.connect();
    connection.query(statement, (error, movies) => {
        connection.end();
        response.send(utils.createResponse(error, movies));
    });
});

router.get('/movie/:id', (request, response) => {
    const id = request.params.id;
    const statement = `select id, title, year, shortDescription, directors, writers, stars, genre, storyline, rating, thumbnail, length from Movie where id = ${id}`;
    const connection = db.connect();
    connection.query(statement, (error, movies) => {
        connection.end();
        response.send(utils.createResponse(error, movies[0]));
    });
});

router.post('/movie', upload.single('thumbnail'), (request, response) => {
    const {title, shortDescription, year, directors, writers, stars, genre, storyline, length, rating} = request.body;
    const connection = db.connect();
    const statement = `insert into Movie
            (title, shortDescription, year, directors, writers, stars, genre, storyline, length, rating, thumbnail) values 
            ('${title}', '${shortDescription}', '${year}', '${directors}', '${writers}', '${stars}', '${genre}', '${storyline}', '${length}', '${rating}', '${request.file.filename}')`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

module.exports = router;
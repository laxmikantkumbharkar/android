package com.sunbeaminfo.myimdb.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.sunbeaminfo.myimdb.R;
import com.sunbeaminfo.myimdb.activity.MovieDetailsActivity;
import com.sunbeaminfo.myimdb.adapter.SearchMoviesAdapter;
import com.sunbeaminfo.myimdb.model.Movie;
import com.sunbeaminfo.myimdb.utils.Constants;
import com.sunbeaminfo.myimdb.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SearchMoviesFragment extends Fragment implements SearchMoviesAdapter.Listener {

    @BindView(R.id.editSearch) EditText editSearch;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;

    ProgressDialog dialog;


    ArrayList<Movie> movies = new ArrayList<>();
    SearchMoviesAdapter adapter;
    Unbinder unbinder;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dialog = new ProgressDialog(getActivity());
        dialog.setTitle("please wait..");
        dialog.setMessage("Searching movies....");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_movie, null);
        unbinder = ButterKnife.bind(this, view);

        adapter = new SearchMoviesAdapter(getActivity(), movies, this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.buttonSearch)
    public void onViewClicked() {
        final String text = editSearch.getText().toString().toLowerCase();
        if (text.length() == 0) {
            Toast.makeText(getActivity(), "enter search text", Toast.LENGTH_SHORT).show();
        } else {

            dialog.show();
            final String url = Utils.getUrl(Constants.ROUTE_MOVIE + "/search/" + text);
            Ion.with(getActivity())
                .load(url)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (result.get("status").getAsString().equals("success")) {
                            movies.clear();

                            JsonArray array = result.get("data").getAsJsonArray();
                            for (int index = 0; index < array.size(); index++) {
                                JsonObject object = array.get(index).getAsJsonObject();

                                Movie movie = new Movie();
                                movie.setId(object.get("id").getAsInt());
                                movie.setTitle(object.get("title").getAsString());
                                movie.setYear(object.get("year").getAsInt());
                                movie.setRating(object.get("rating").getAsFloat());
                                movie.setThumbnail(object.get("thumbnail").getAsString());
                                movie.setLength(object.get("length").getAsString());

                                movies.add(movie);
                            }

                            adapter.notifyDataSetChanged();

                            dialog.dismiss();
                        } else {
                            Toast.makeText(getActivity(), "Error while loading the movies", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        }
    }

    @Override
    public void onClick(int index) {
        Movie movie = movies.get(index);

        Intent intent = new Intent(getActivity(), MovieDetailsActivity.class);
        intent.putExtra("id", movie.getId());
        startActivity(intent);
    }
}

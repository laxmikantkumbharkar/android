package com.sunbeaminfo.myimdb.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;
import com.sunbeaminfo.myimdb.R;
import com.sunbeaminfo.myimdb.model.Movie;
import com.sunbeaminfo.myimdb.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.ViewHolder> {

    public interface Listener {
        public void onClick(int index);
    }

    private final Context context;
    private final ArrayList<Movie> movies;

    private final Listener listener;

    public MoviesAdapter(Context context, ArrayList<Movie> movies, Listener listener) {
        this.context = context;
        this.movies = movies;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.recyclerview_item_movie, null));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        Movie movie = movies.get(i);
        viewHolder.textTitle.setText(movie.getTitle());
        viewHolder.textYear.setText("" + movie.getYear());
        viewHolder.textLength.setText(movie.getLength());
        viewHolder.textRating.setText("" + movie.getRating());

        viewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(i);
            }
        });

        String imageUrl = Utils.getImageUrl(movie.getThumbnail());
        Ion.with(context)
            .load(imageUrl)
            .withBitmap()
            .intoImageView(viewHolder.imageView);
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageView) ImageView imageView;
        @BindView(R.id.textTitle) TextView textTitle;
        @BindView(R.id.textYear) TextView textYear;
        @BindView(R.id.textLength) TextView textLength;
        @BindView(R.id.textRating) TextView textRating;

        View view;

        ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
    }
}

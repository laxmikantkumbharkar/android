package com.example.dairy.model;

public class Product {
    private int id;
    private String name;
    private String category;
    private String quantity;
    private float rate;
    private String thumbnail;

    public Product() {
    }

    public Product(int id, String name, String category, String quantity, float rate, String thumbnail) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.quantity = quantity;
        this.rate = rate;
        this.thumbnail = thumbnail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", category='" + category + '\'' +
                ", quantity='" + quantity + '\'' +
                ", rate=" + rate +
                ", thumbnail='" + thumbnail + '\'' +
                '}';
    }
}

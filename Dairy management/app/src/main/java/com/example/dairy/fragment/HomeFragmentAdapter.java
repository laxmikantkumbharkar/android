package com.example.dairy.fragment;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.dairy.adapter.ButterListFragment;
import com.example.dairy.adapter.CurdListFragment;
import com.example.dairy.adapter.IcecreamListFragment;
import com.example.dairy.adapter.MilkListFragment;
import com.example.dairy.adapter.PaneerListFragment;

public class HomeFragmentAdapter extends FragmentPagerAdapter {
    public HomeFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    ButterListFragment butterListFragment=new ButterListFragment();
    CurdListFragment curdListFragment=new CurdListFragment();
    IcecreamListFragment icecreamListFragment=new IcecreamListFragment();
    MilkListFragment milkListFragment=new MilkListFragment();
    PaneerListFragment paneerListFragment=new PaneerListFragment();

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        String title="";
        if(position == 0){
            title="Ice-cream";
        }else if(position == 1){
            title="Milk";
        }else if(position == 2){
            title="Butter";
        }else if(position == 3){
            title="Curd";
        }else if(position == 4){
            title="Paneer";
        }
        return title;
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment=null;
        if( i == 0){
            fragment=icecreamListFragment;
        }else if( i == 1){
            fragment=milkListFragment;
        }else if ( i == 2){
            fragment=butterListFragment;
        }else if(i == 3){
            fragment=curdListFragment;
        }else if(i == 4){
            fragment=paneerListFragment;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 5;
    }
}

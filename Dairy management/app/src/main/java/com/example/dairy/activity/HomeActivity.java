package com.example.dairy.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.example.dairy.R;
import com.example.dairy.fragment.HomeFragmentAdapter;
import com.example.dairy.fragment.ProductListFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.tabLayout) TabLayout tabLayout;
    @BindView(R.id.viewPager) ViewPager viewPager;

    HomeFragmentAdapter homeFragmentAdapter;
    ProductListFragment productListFragment=new ProductListFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        getSupportActionBar().hide();

        homeFragmentAdapter=new HomeFragmentAdapter(getSupportFragmentManager());
        viewPager.setAdapter(homeFragmentAdapter);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setIcon(R.drawable.icecream);
        tabLayout.getTabAt(1).setIcon(R.drawable.milk);
        tabLayout.getTabAt(2).setIcon(R.drawable.butter);
        tabLayout.getTabAt(3).setIcon(R.drawable.curd);
        tabLayout.getTabAt(4).setIcon(R.drawable.paneer);

    }

    private BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener
            =new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            switch (menuItem.getItemId()){
                case R.id.navigation_home:

            }
            return false;
        }
    };

}
